import { format } from 'date-fns';
import Calendar from 'react-calendar';
import fr from 'date-fns/esm/locale/fr/index.js';
import { useState } from 'react';

import './App.css';

function App() {
  // Mon hook pour afficher la date cliquée sur le calandar
  const [dateClicked, setDateClicked] = useState(new Date());

  const dateFormated = format(dateClicked, 'dd MMMM yyyy', { locale: fr });

  // Mon hook pour afficher ou non le calandar
  const [showCalandar, setShowCalandar] = useState(true);

  return (
    <div className='App'>
      <button
        className='calandarButton'
        type='button'
        onClick={() => setShowCalandar(!showCalandar)}
      >
        {dateClicked ? dateFormated : 'date'}
      </button>
      <div className={showCalandar === true ? 'hideCalandar' : 'showCalandar'}>
        <Calendar
          onChange={(e) => {
            setDateClicked(e), setShowCalandar(!showCalandar);
          }}
          value={dateClicked}
        />
      </div>
    </div>
  );
}

export default App;
